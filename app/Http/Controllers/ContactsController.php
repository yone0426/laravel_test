<?php

namespace App\Http\Controllers;
//lluminate\Http\Requestインスタンスは、Symfony\Component\HttpFoundation\Requestクラスを拡張しており、HTTPリクエストを調べるために数多くのメソッドを提供しています。
use App\Http\Requests\SampleRequest;
use Illuminate\Http\Request;
use App\Contact;
// use Illuminate\Database\Eloquent\Model;
// use Contact;
// use Validator;

class ContactsController extends Controller
{
    
    public function input(){
        // echo 3333333;exit;
        return view('input');
    }

    ///リクエスト（Request）のあれこれ Laravelで、リクエスト(Request)というのは、ブラウザを通してユーザーから送られる情報をすべて含んでいるオブジェクトのことです。
    public function confirm(SampleRequest $request){
        $inputs = $request->all();
        // dd($inputs);
        $request->session()->put('inputs',$inputs);
        // return view('confirm', compact('inputs'));  
        return view('confirm', compact('inputs'));
        // dump($_POST);
        // dump($errors);
        // dump($name);
        // var_dump($inputs);
        // var_dump($this);
        // if(empty($errors)){
        //     return view('confirm',compact('name', 'email', 'inquiry'));
        // }else{
        //     return view('input',compact('errors','name', 'email', 'inquiry'));
        // }
    }
    
    public function complete(Request $request){
        $inputs=$request->session()->get('inputs');
        // dd($inputs)
        // $contact = new App\Contact();
        $contact = new Contact;
        // dd($contact);
      
        $contact->name = $inputs['name'];
        $contact->email = $inputs['email'];
        $contact->inquiry = $inputs['inquiry'];
        // $contact->inquiry = $request->inquiry;
        $contact->save();
        return view('complete', compact('inputs'));  
        // $name = $request->session()->get('name');
        // $email = $request->session()->get('email');
        // $inquiry = $request->session()->get('inquiry');
        // return view('complete', compact('name', 'email', 'inquiry'));
    }
    /* ----------------------------------------------------------------- 
    vali3
    ----------------------------------------------------------------- */
    // $name = $request->input('name');
    // $email = $request->input('email');
    // $inquiry = $request->input('inquiry');
    // $request->session()->put('name',$name);
    // $request->session()->put('email',$email);
    // $request->session()->put('inquiry',$inquiry);
    // // $errors = array();
    // if(isset($_POST)){
    //     //   $errors = $this->vali($name,$email,$inquiry);
    //       $this->vali($name,$email,$inquiry);
    //     }
    //     if(empty($this->errors)){
    //       return view('confirm',compact('name', 'email', 'inquiry'));
    //     }else{
    //       $errors = $this->errors;
    //       return view('input',compact('errors','name', 'email', 'inquiry'));
    //     }
    // private function vali($name, $email, $inquiry){
    //     if(empty($errors)){
    //       if(empty($name)){
    //           $this->errors[]="*名前を入力してください。";
    //       }
    //       if(empty($email)){
    //           $this->errors[]="*アドレスを入力してください。";
    //       }
    //       if(empty($inquiry)){
    //           $this->errors[]="*お問い合わせを入力してください。";
    //       }
    //     }
    //   }

    // 4
    // $inputs = $request->all();
    // $request->session()->put('inputs',$inputs);
    // // return view('confirm', compact('inputs'));  
    // $rules = ['name'=>'required',
    //           'email'=>'required|email',
    //           'inquiry' => 'required'];

    // $error_message = ['required'=>'必須です',
    //                     'email'=>'アドレスをメール形式で入力してください'];
    // $this->validate($request, $rules, $error_message);                    
    // return view('confirm', compact('inputs'));
    // 5
    // $rules = ['name'=>'required',
    //               'email'=>'required|email',
    //               'inquiry' => 'required'];

    //     $error_message = ['required'=>'必須です',
    //                       'email'=>'アドレスをメール形式で入力してください'];
    //     $validator = Validator::make($request->all(), $rules, $error_message);
    //     if($validator ->fails()){
    //       return redirect('/input')->withErrors($validator)->withInput();
    //     }
}