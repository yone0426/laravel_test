<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function register(){
        return view('register');
    }
    public function login(){
        $user = new User;
        $users = User::all();
        // dd($users);
        return view('login');
    }
    public function list2(Request $request){
        $registers = $request->all();
        $rules = 
        ['password'=>'required',
        'email'=>'required|email'];
        $error_message = ['required'=>'必須項目です',
                          'email'=>'アドレス形式で入力してください'];
        $validator = Validator::make($request->all(), $rules, $error_message);
        if($validator ->fails()){
            return redirect('/login')->withErrors($validator)->withInput();
        }
    }

    public function store(Request $request){
        $registers = $request->all();
        $request->session()->put('registers',$registers);
        $rules = ['name'=>'required',
                  'password'=>'required',
                  'email'=>'required|email'];
        $error_message = ['required'=>'必須項目です',
                           'email'=>'アドレス形式で入力してください'];
        $validator = Validator::make($request->all(), $rules, $error_message);
        // var_dump($registers);
        if($validator ->fails()){
            return redirect('/register')->withErrors($validator)->withInput();
        }
        $user = new User;
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->password = $request['password'];
        $user->save();
        // dd($user);
        // echo 3333333;exit;
        return redirect('list');
    }    

    public function list(Request $request){
        $registers = $request->session()->get('registers');
        // if (isset($registers)){
            $user = new User;
            $users = User::all();
        // } else {
        //     return redirect('login');
        // }
        return view('list', compact('users'));         
    }


    public function show($id) {
        $admin = User::find($id);
        return view('show' , compact('admin'));
        // echo 3333333;exit;
    }
    
    public function edit(Request $request, $id) {
        $registers = $request->session()->get('registers');
        $admin = User::find($id);
        
            return view('edit' , compact('admin'));
            // $user = new User;
            // $users = User::all();
            // return view('list', compact('users'));         
       
    }
   
    public function destroy(Request $request, $id){
        $registers = $request->session()->get('registers');
        $admin = User::find($id);
        // $email_r = $registers['email'];
        // $email_a = $admin['email'];
        // if ($email_r === $email_a){
          return view('destroy' , compact('admin'));
        // } else {
        //     return redirect('login');
        // } 
    }
    public function delete($id) {
        $admin = User::find($id);
        $admin->delete();
        return redirect('list');
    }

    public function update(Request $request, $id){
        $admin = User::find($id);
        $registers = $request->all();
        $request->session()->put('registers',$registers);
        $rules = ['name'=>'required',
        'password'=>'required'];
        $error_message = ['required'=>'必須項目です'];
                                                   
        $validator = Validator::make($request->all(), $rules, $error_message);
        //  echo 3333333;exit;
        // dd($admin);
        if($validator ->fails()){
            return redirect(url('/edit',['id' =>$admin->id]))->withErrors($validator)->withInput();
        }
        $admin->name = $request['name'];
        $admin->password = $request['password'];
        // dd($admin);
        return view('update' , compact('admin'));
    }

    public function store2(Request $request,$id){
        // $r = $request->all();
        // $registers = $request->all();
        $admin = User::find($id);
        $admin->name = $request['name'];
        $admin->password = $request['password'];
        $admin->save();
        // dd($admin);
        // echo 3333333;exit;
        return redirect('list');
    }

    public function login2(Request $request){
        $registers = $request->all();
       
        $rules = [
            'password'=>'required',
            'email'=>'required|email'];
        $error_message = [
            'required'=>'必須項目です',
            'email'=>'アドレス形式で入力してください'];
        $validator = Validator::make($request->all(), $rules, $error_message);
        if($validator ->fails()){
            return redirect('/login')->withErrors($validator)->withInput();
        }

        if (isset($request->email)){
            $param = user::where('email', $request->email)->first();
            $pass = $param['password'];
            $p = $request->password;
            if ($pass === $p){
                $registers = $request->all();
                $request->session()->put('registers',$registers);
               
                return  redirect('/list');
            } else {
                echo '正しいアドレスとパスワードを入力ください';
                return redirect('/login');
            }
        }
    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('/login');
    }
}
