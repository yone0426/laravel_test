<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/input', 'ContactsController@input');
Route::post('/confirm', 'ContactsController@confirm');
Route::post('/complete', 'ContactsController@complete');

// Route::post('/input', 'ContactsController@input_2');
// Route::resource('posts', 'ContactsController');
Route::get('/register', 'UserController@register');
Route::get('/list', 'UserController@list');
Route::post('/store', 'UserController@store');
Route::post('/store2/{id}', 'UserController@store2');
Route::post('/delete/{id}', 'UserController@delete');
Route::get('/login', 'UserController@login');
Route::get('/show/{id}', 'UserController@show');
Route::get('/edit/{id}', 'UserController@edit');
Route::post('/update/{id}', 'UserController@update');
Route::get('/destroy/{id}', 'UserController@destroy');
Route::post('/list', 'UserController@list');
Route::post('/login2', 'UserController@login2');
Route::post('/logout', 'UserController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
