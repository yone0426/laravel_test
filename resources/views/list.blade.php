
<!DOCTYPE html>
<html>
  <body>
    <h1>一覧画面</h1>
      <br>
      <table border="1">
        <thead>
          <tr>
            <th>id</th>
            <th>name</th>
            <th>email</th>
            <th>修正</th>
            <th>削除</th>
            <th>詳細</th>
          </tr>
        </thead>
        @foreach ($users as $user)
        <tbody>
          <tr>
          <div>  
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td><form method='get' action="{{url('/edit',['id' =>$user->id])}}">
                  {{ csrf_field() }}
                  <input type="submit" value="修正">
                </form></td>
            <td><form method='get' action="{{url('/destroy',['id' =>$user->id])}}">
                  {{ csrf_field() }}
                  <input type="submit" value="削除">
                </form></td>
            <td><a href="{{url('/show',['id' =>$user->id])}}">詳細</a></td>    
          </div>                      
          </tr>
        </tbody>
        @endforeach  
      </table>  <br>
            <!-- <td><a href="{{url('/login')}}">ログイン画面へ</a></td>     -->
            <form method='post' action="{{url('/logout')}}">
                {{ csrf_field() }}
                <input type="submit" value="ログアウト">
            </form>  
            <form method='get' action="{{url('/home')}}">
                {{ csrf_field() }}
                <input type="submit" value="Home">
            </form>  
  </body>
</html>

