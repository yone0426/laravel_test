<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>

  <body>

    <h1>修正画面</h1>
    <form action="{{url('/update',['id' =>$admin->id])}}" method="post">
      {{ csrf_field() }}
      名前<br>
      @if($errors->has('name'))
        <div class="error">
          <p>{{ $errors->first('name') }}</p>
        </div>
      @endif
      <input type="text" name="name" value="{{$admin->name}}"><br>
      パスワード<br>
      @if($errors->has('password'))
        <div class="error">
          <p>{{ $errors->first('password') }}</p>
        </div>
      @endif
      <input type="text" name="password" value="{{$admin->password}}"><br><br>
      <input type="button" name="btn_back" value="戻る" onclick="history.back()">
      <input type="submit" value="修正確認画面へ" name="send">
      <!-- <a href="{{ url('/login') }}">ログイン画面へ</a> -->
    </form>
 </body>
</html>





