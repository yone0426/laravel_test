<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>

  <body>
    <h1>ログイン画面</h1>
    <form action="{{ url('/login2') }}" method="post">
      {{ csrf_field() }}
      アドレス<br>
      @if($errors->has('email'))
        <div class="error">
          <p>{{ $errors->first('email') }}</p>
        </div>
      @endif
      <input type="text" name="email" value="{{old('email')}}"><br>
      パスワード<br>
      @if($errors->has('password'))
        <div class="error">
          <p>{{ $errors->first('password') }}</p>
        </div>
      @endif
      <input type="text" name="password" value=""><br><br>
      
      <input type="submit" value="ログイン" name="send">
      <a href="{{ url('/register') }}">登録画面へ</a>
    </form>
 </body>
</html>

