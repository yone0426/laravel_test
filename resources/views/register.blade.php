<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>

  <body>

    <h1>登録画面</h1>
    <form action="{{ url('/store') }}" method="post">
      {{ csrf_field() }}
      名前<br>
      @if($errors->has('name'))
        <div class="error">
          <p>{{ $errors->first('name') }}</p>
        </div>
      @endif
      <input type="text" name="name" value="{{old('name')}}"><br>
      アドレス<br>
      @if($errors->has('email'))
        <div class="error">
          <p>{{ $errors->first('email') }}</p>
        </div>
      @endif
      <input type="text" name="email" value="{{old('email')}}"><br>
      パスワード<br>
      @if($errors->has('password'))
        <div class="error">
          <p>{{ $errors->first('password') }}</p>
        </div>
      @endif
      <input type="text" name="password" value="{{old('password')}}"><br><br>
      <input type="submit" value="登録" name="send">
      <a href="{{ url('/login') }}">ログイン画面へ</a>
    </form>
 </body>
</html>





