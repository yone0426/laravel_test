<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>

  <body>

    <h1>お問い合わせ</h1>
    <form action="{{ url('/confirm') }}" method="post">
    <!-- Laravelでは、クロス・サイト・リクエスト・フォージェリ(CSRF)からアプリケーションを簡単に守れます。クロス・サイト・リクエスト・フォージェリは悪意のあるエクスプロイトの一種であり、信頼できるユーザーになり代わり、認められていないコマンドを実行します。 -->
    <!-- アプリケーションでHTMLフォームを定義する場合はいつでも、隠しCSRFトークンフィールドをフォームに埋め込み、CSRF保護ミドルウェアがリクエストの有効性をチェックできるようにしなければなりません。 -->
      {{ csrf_field() }}
      <!-- @if ($errors->any())
        <div class='error_message'>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
             @endforeach
          </ul>
        </div>
      @endif -->
      名前<br>
      @if($errors->has('name'))
        <div class="error">
          <p>{{ $errors->first('name') }}</p>
        </div>
      @endif
      <input type="text" name="name" value="{{old('name')}}"><br>
      アドレス<br>
      @if($errors->has('email'))
        <div class="error">
          <p>{{ $errors->first('email') }}</p>
        </div>
      @endif
      <input type="text" name="email" value="{{old('email')}}"><br>
      お問い合わせ<br>
      @if($errors->has('inquiry'))
        <div class="error">
          <p>{{ $errors->first('inquiry') }}</p>
        </div>
      @endif
      <input type="text" name="inquiry" value="{{old('inquiry')}}"><br>
      <input type="submit" value="送信" name="send">
    </form>
 </body>
</html>
