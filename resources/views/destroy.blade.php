<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>

  <body>

    <h1>削除確認画面</h1>
    <form action="{{ url('/delete',['id' =>$admin->id]) }}" method="post">
      {{ csrf_field() }}
      <p>名前</p>
      <input type="hidden" name="name" value="{{$admin->name}}"><br>
      {{$admin->name}}
      <br>
      <p>メールアドレス</p>
      <input type="hidden" name="email" value="{{$admin->email}}">
      {{$admin->email}}<br>
      <input type="submit" value="削除" name="send">
      <a href="{{ url('/login') }}">ログイン画面へ</a>
    </form>
 </body>
</html>





